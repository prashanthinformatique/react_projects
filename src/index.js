import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./app.scss";

// import Project_A from './projects/project_a/parent';
// import Project_B from './projects/project_b/parent';
// import Project_C from './projects/project_c/parent';
// import Project_D from './projects/project_d/parent';
// import ProjectE from "./projects/project_e";
// import ProjectF from "./projects/project_f";
// import ProjectG from "./projects/project_g";
// import ProjectH from "./projects/project_h";
// import ProjectJ from "./projects/project_j";
// import ProjectK from "./projects/project_k";
// import ProjectL from "./projects/project_l";
import ProjectM from "./projects/project_m";

import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<ProjectM />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
