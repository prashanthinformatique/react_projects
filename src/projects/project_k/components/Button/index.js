import React from 'react';
import classes from './button.module.scss';

const Button = ({ children, theme, space = '', onClick }) => {

    
    const setTheme = (theme) => {
        switch(theme){
            case 'primary':
                return classes.primary;
            case 'critical':
                return classes.critical;
        }
    }

    return ( <>
		<button 
            className={`${classes.btn_default} ${setTheme(theme)} ${space && classes.space }`}
            onClick={onClick}>
                {children}
        </button>
    </> );
}
 
export default Button;