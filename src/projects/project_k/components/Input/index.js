import React, { useEffect, useRef } from 'react';
import classes from './input.module.scss';

const Input = ({ type, name , id, onChange , onFocus }) => {

    const inputEl = useRef(null);


    const handleChange = (e) => {
        onChange( parseInt(e.target.value) )
        // inputEl.reset();
        // e.target.value = '';
        
    }

    // useEffect(() => {
    //     if( reset )
    //      document.getElementById(id).value = '';
    // },[reset])

    return ( <>
        <div className={classes.form_field}>
            <input 
                onFocus={onFocus}
                ref={inputEl}
                type={type} 
                id={id} 
                name={name} 
                onChange={ (e) => handleChange(e)  }
            />
        </div>
    </> );
}
 
export default Input;