import React from 'react';
import classes from './box.module.scss';

const Box = ({ children, center='' }) => {
    return ( 
        <>
            <div className={`${classes.box} ${center && classes.center}`}>
                {children}
            </div>
        </>
     );
}
 
export default Box;