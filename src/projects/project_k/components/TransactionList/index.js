import React, { useEffect } from 'react';
import classes from './transaction.module.scss';

const TransactionList = ({ data }) => {
    return ( <>
        <div className={classes.transaction_list}>
            { data.map( row => <span key={row.time}>{row.time} - {row.amount} - {row.action}</span> ) }
        </div>
    </> );
}
 
export default TransactionList;