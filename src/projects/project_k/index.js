import React, { useState, useEffect } from "react";
import Box from "./components/Box";
import Button from "./components/Button";
import Input from "./components/Input";
import TransactionList from "./components/TransactionList";

const ProjectK = () => {

	const [data , setData ] = useState([
		{
			time: '2021-03-10T15:04:02.072Z',
			amount: '100',
			action: 'Add'
		}
	]);

	const [ balance , setBalance ] = useState(0);
	const [ value, setValue ] = useState(0);

	// const [ reset, setReset ] = useState(false);


	const handleValue = (val) => {
		setValue(val);
	}

	const addValue = () => {
		setBalance( balance + value );
		updateTransaction( value, 'Add' );
	}

	const removeValue = () => {
		setBalance( balance - value );
		updateTransaction( value, 'Remove' );
	}

	const updateTransaction = ( value, action ) => {

		let date  = new Date();

		// console.log(date.getMonth() < 10 ? '0'+date.getMonth(): date.getMonth());

		data.push( 
			{
				time: String(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}T${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}Z`),
				amount: value,
				action: action
			}
		)
		setData(data);
		// setReset(true);
	}


	return (
		<React.Fragment>
			<h4 className="heading">Expense Tracker</h4>
			<Box center={true}>
				<h5>Balance : {balance}</h5>
				<Input type="number" id="tracker" name="tracker" onChange={handleValue} 
				  />
				<Button theme="primary" space={true} onClick={addValue}>Add</Button>
				<Button theme="critical" onClick={removeValue}>Remove</Button>
			</Box>
			<Box>
				<h6>Transactions</h6>
				<TransactionList data={data} />
			</Box>
		</React.Fragment>
	);
};

export default ProjectK;
