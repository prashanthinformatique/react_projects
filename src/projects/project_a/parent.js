import React, { Component } from 'react';
import Collapses from './components/collapses';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import './project_a.scss';

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
	const result = Array.from(list);
	const [ removed ] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);
	return result;
};

const getItemStyle = (draggableStyle, isDragging) => ({
	// some basic styles to make the items look a bit nicer
	userSelect: 'none',
	// change background colour if dragging
	background: isDragging ? '' : '',

	// styles we need to apply on draggables
	...draggableStyle
});
const getListStyle = (isDraggingOver) => ({
	background: isDraggingOver ? '' : ''
});

class Project_A extends Component {
	state = {
		collapse_common: false,
		collapse_status: false,
		collapse_id: '',
		// collapses: [
		// 	{ id: 1, title: 'Sliding Banners', collapse_status: false, block_status: true },
		// 	{ id: 2, title: 'Top Categories', collapse_status: false, block_status: true },
		// 	{ id: 3, title: 'Image Grid', collapse_status: false, block_status: true },
		// 	{ id: 4, title: 'Banner Images', collapse_status: false, block_status: true }
		// ],
		collapses: [],
		cards: [ { id: 1 }, { id: 2 }, { id: 3 } ],
		apiData: []
	};

	componentDidMount() {
		fetch('https://www.mocky.io/v2/5e8a7b512d00003c1a1a4665').then((result) => result.json()).then((json) => {
			console.log(json);
			const collapses = json.homepage_data.map((value, key) => {
				return { ...value, collapse_status: false, block_status: true, id: key + '1' };
			});
			this.setState({
				collapses
			});
		});
	}

	expandCollapse = (id) => {
		const { collapse_id, collapses } = this.state;
		if (collapse_id == id) {
			collapses.find((element) => element.id == id).collapse_status = !collapses.find(
				(element) => element.id == id
			).collapse_status;
		} else {
			collapses.find((element) => element.id == id).collapse_status = true;
			if (collapse_id) collapses.find((element) => element.id == collapse_id).collapse_status = false;
		}
		// console.log(collapses);
		this.setState({ collapse_id: id, collapses }, () => {
			// console.log(this.state.collapse_id, this.state.collapses)
		});
	};

	showHide = (id) => {
		// console.log('show/hide');
		const { collapse_id, collapses } = this.state;

		if (collapse_id == id) {
			collapses.find((element) => element.id == id).block_status = !collapses.find((element) => element.id == id)
				.block_status;
		}
		this.setState({ collapses });
	};

	onDelete = (id) => {
		const collapses = this.state.collapses.filter((element) => element.id !== id);
		this.setState({
			collapses,
			collapse_id: ''
		});
	};

	onDuplicate = (id) => {
		const { collapses } = this.state;
		let currentLength = collapses.length; // 4
		const curentObject = collapses.find((element) => element.id == id);
		const duplicateObject = JSON.parse(JSON.stringify(curentObject));
		duplicateObject.id = currentLength + 1;
		duplicateObject.collapse_status = false;
		let currentIndex = id - 1;
		collapses.splice(currentIndex, 0, duplicateObject);
		this.setState({
			collapses
		});
	};

	onDragEnd = (result) => {
		// dropped outside the list
		if (!result.destination) {
			return;
		}
		const { collapses } = this.state;
		const items = reorder(collapses, result.source.index, result.destination.index);
		this.setState({
			collapses: items
		});
	};

	render() {
		return (
			<React.Fragment>
				<div className="container">
					<div className="section_block">
						<DragDropContext onDragEnd={this.onDragEnd}>
							<Droppable droppableId="droppable">
								{(provided, snapshot) => (
									<div
										ref={provided.innerRef}
										style={getListStyle(snapshot.isDraggingOver)}
										{...provided.droppableProps}
									>
										{this.state.collapses.map((collapse, index) => (
											<Draggable
												// adding a key is important!
												key={collapse.id}
												draggableId={collapse.id}
												index={index}
											>
												{(provided, snapshot) => (
													<div
														ref={provided.innerRef}
														{...provided.draggableProps}
														{...provided.dragHandleProps}
														style={getItemStyle(
															provided.draggableProps.style,
															snapshot.isDragging
														)}
													>
														<Collapses
															key={collapse.id}
															id={collapse.id}
															collapse={collapse}
															propsExpandCollapse={this.expandCollapse}
															propsShowHide={this.showHide}
															propsOnDelete={this.onDelete}
															propsOnDuplicate={this.onDuplicate}
															cards={this.state.cards}
														/>
													</div>
												)}
											</Draggable>
										))}
									</div>
								)}
							</Droppable>
						</DragDropContext>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Project_A;
