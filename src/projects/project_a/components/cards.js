import React, { Component } from 'react';

class Card extends Component {
	render() {
		return (
			<React.Fragment>
				<div className="cic_card">
					<div className="img">
						<img src={this.props.card.image_url} alt="" />
					</div>
					<div className="thumbnail_block">
						<button disabled={this.props.collapse.block_status == true ? false : true}>Upload</button>
						<button disabled={this.props.collapse.block_status == true ? false : true}>Edit</button>
						<button disabled={this.props.collapse.block_status == true ? false : true}>Delete</button>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Card;
