import React, { Component } from 'react';
import Card from './cards';

class Collapses extends Component {
	getShowHideStatus() {
		let button_show_hide = 'expd_btn hide_show_btn';
		button_show_hide += this.props.collapse.block_status == true ? ' hide' : ' show';
		return button_show_hide;
	}

	render() {
		return (
			<React.Fragment>
				<div
					className={
						this.props.collapse.collapse_status == true ? (
							'collapse_block active'
						) : (
							'collapse_block inactive'
						)
					}
					id={this.props.id}
				>
					<div className="cb_single">
						<div className="cb_img">
							<img src={require('../images/dots_gray.png')} alt="" />
						</div>
						<div className="cb_content">
							<div className="cb_head">
								<h4>{this.props.collapse.title || this.props.collapse.name}</h4>
							</div>
							<div className="cb_btn">
								<button
									className="expd_btn duplicate_btn"
									onClick={() => this.props.propsOnDuplicate(this.props.id)}
								>
									Duplicate
								</button>
								<button
									className="expd_btn delete_btn"
									onClick={() => this.props.propsOnDelete(this.props.id)}
								>
									Delete
								</button>
								<button
									className={this.getShowHideStatus()}
									onClick={() => this.props.propsShowHide(this.props.id)}
								>
									{this.props.collapse.block_status == true ? 'Hide' : 'Show'}
								</button>
								<button
									className="expd_btn expand_btn"
									onClick={() => this.props.propsExpandCollapse(this.props.id)}
								>
									Expand
								</button>
							</div>
						</div>
					</div>
					<div
						className={
							this.props.collapse.block_status == true ? (
								'collapse_inside_content show'
							) : (
								'collapse_inside_content hide'
							)
						}
					>
						{this.props.collapse.data.map((card, key) => (
							<Card key={key} id={key} collapse={this.props.collapse} card={card} />
						))}
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Collapses;
