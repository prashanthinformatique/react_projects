import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import '../../app.scss';
import './project_c.scss';
import hamburger from '../../assets/images/hamburger.png';
import Logo from '../../assets/images/projects_c/logo.png';
import profileIcon from '../../assets/images/projects_c/profile_icon.png';
import formImage from '../../assets/images/projects_c/form_image.png';

class Project_C extends Component {
	state = {
		hamburger: true
	};

	ham_menu = () => {
		this.setState({
			hamburger: this.state.hamburger == false ? true : false
		});
	};

	render() {
		return (
			<React.Fragment>
				<header className="header">
					<div className="logo">
						<a href="#">
							<img src={Logo} />
						</a>
					</div>
					<ul className={this.state.hamburger == true ? 'menu toggled' : 'menu'}>
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">Credentialing Organizations</a>
						</li>
						<li>
							<a href="#">Hospitals</a>
						</li>
						<li>
							<a href="#">Reports</a>
						</li>
					</ul>
					<div className="profile">
						<div className="ham_icon">
							<img src={hamburger} onClick={this.ham_menu} />
						</div>
						<div className="profile_tag">
							<img src={profileIcon} />
							<span className="username">Ericnel</span>
						</div>
					</div>
				</header>

				<section className="banner" id="banner">
					<div className="banner_img">
						<div className="container">
							<div className="banner_content">
								<h1>Add Credentialing Organisation</h1>
								<p>Onboard A New Credentialing Organisation</p>
							</div>
						</div>
					</div>
					<div className="form_block">
						<div className="container">
							<div className="fb_row">
								<div className="form_block_left">
									<div className="fbl_img">
										<img src={formImage} />
									</div>
								</div>
								<div className="form_block_right">
									<div className="fbr_form_elements">
										<div className="fbr_form_group">
											<label>Organisation Name*</label>
											<input type="text" placeholder="University of California" />
										</div>
										<div className="fbr_form_group">
											<label>Country*</label>
											<select name="country" id="country">
												<option value="USA">USA</option>
												<option value="INDIA">INDIA</option>
											</select>
										</div>
										<div className="fbr_form_group full_width">
											<label>Email Address*</label>
											<input type="email" placeholder="skinner@support.com" />
										</div>
										<div className="fbr_form_group">
											<label>Certificate Category*</label>
											<select name="certificate_categ" id="certificate_categ">
												<option value="demographics">Demographics</option>
												<option value="Certificate 2">Certificate 2</option>
											</select>
										</div>
										<div className="fbr_form_group">
											<label>Certificate*</label>
											<select name="certificate" id="certificate">
												<option value="SSN">SSN</option>
												<option value="MSN">MSN</option>
											</select>
										</div>
										<div className="fbr_form_group form_submit">
											<input type="submit" value="Add" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<footer>
					<div className="left_footer">
						<a href="#">
							<span className="company_name">MedCREDS</span>
						</a>
					</div>
					<div className="right_footer">
						<span className="copy_rights">Copyright © MedCreds 2020</span>
					</div>
				</footer>
			</React.Fragment>
		);
	}
}

export default Project_C;
