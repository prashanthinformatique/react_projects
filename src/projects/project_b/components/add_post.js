import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';

class AddPost extends Component {
	state = {
		newPost: {
			title: '',
			body: ''
		}
	};

	componentDidMount() {
		const id = this.props.history.location.pathname.split('/')[2];
		console.log('id', id);
		if (id) {
			console.log(this.props);
			const newPost = this.props.posts.find((post) => post.id == id);
			console.log(newPost);
			this.setState({
				newPost
			});
		}
		console.log(this.props.history.location.pathname.split('/')[2]);
	}

	handleChange = (e) => {
		const name = e.target.name;
		const value = e.target.value;
		const { newPost } = this.state;
		newPost[name] = value;
		this.setState({
			newPost
		});
	};

	handleSubmit = (e) => {
		e.preventDefault();
		const newPost = this.state.newPost;
		const id = this.props.history.location.pathname.split('/')[2];
		if (id) {
			newPost.id = id;
			this.props.parentEditCallback(newPost);
		} else {
			this.props.parentCallback(newPost);
		}
	};

	render() {
		return (
			<React.Fragment>
				<h1>Add New Post</h1>
				<form onSubmit={this.handleSubmit}>
					<div>
						<div>
							<label>Title</label>
						</div>
						<input type="text" value={this.state.newPost.title} name="title" onChange={this.handleChange} />
					</div>
					<div>
						<div>
							<label>Post Content</label>
						</div>
						<textarea rows="5" value={this.state.newPost.body} name="body" onChange={this.handleChange} />
					</div>
					<div>
						<input type="submit" id="submit_post" value="Submit" />
					</div>
				</form>
			</React.Fragment>
		);
	}
}

export default withRouter(AddPost);
