import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, withRouter } from 'react-router-dom';

class HomePage extends Component {
	OnEdit = (id) => {
		console.log(id);
		this.props.history.push(`/edit/${id}`);
	};
	render() {
		return (
			<React.Fragment>
				<header>
					<Link to="/add_new" className="add_new_post">
						Add New
					</Link>
					{this.props.propsPosts.map((post) => (
						<div key={post.id} post-id={post.id}>
							<h3>{post.title}</h3> <p>{post.body}</p>
							<button onClick={() => this.OnEdit(post.id)} post-id={post.id}>
								Edit
							</button>
							<button onClick={() => this.props.propsOnDelete(post.id)} post-id={post.id}>
								Delete
							</button>
						</div>
					))}
				</header>
			</React.Fragment>
		);
	}
}

export default withRouter(HomePage);
