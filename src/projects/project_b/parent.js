import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import HomePage from './components/homepage';
import AddPost from './components/add_post';
import '../../app.scss';
import './project_b.scss';

class Project_B extends Component {
	state = {
		posts: [],
		isLoaded: false,
		title: '',
		post_content: '',
		redirect: false
	};

	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/posts').then((res) => res.json()).then((json) => {
			this.setState({
				isLoaded: true,
				posts: json
			});
		});
	}

	callBackFunction = (newPost) => {
		const postsLength = this.state.posts.length;
		newPost.id = postsLength + 1;
		newPost.userId = 1;
		const { posts } = this.state;
		posts.unshift(newPost);
		this.setState(
			{
				posts,
				redirect: true
			},
			() => {
				this.setState({
					redirect: false
				});
			}
		);
	};

	callBackEditFunction = (newPost) => {
		const { posts } = this.state;
		const result = posts.find((post) => post.id == newPost.id);
		result.title = newPost.title;
		result.body = newPost.body;
		this.setState(
			{
				posts,
				redirect: false
			},
			() => {
				this.setState({
					redirect: true
				});
			}
		);
	};

	renderRedirect = () => {
		if (this.state.redirect) {
			return <Redirect to="/" />;
		}
	};

	onDelete = (rowId) => {
		const posts = this.state.posts.filter((post) => post.id !== rowId);
		this.setState({
			posts
		});
	};

	render() {
		return (
			<React.Fragment>
				<Router>
					{this.renderRedirect()}
					<Route
						exact
						path="/"
						component={() => <HomePage propsOnDelete={this.onDelete} propsPosts={this.state.posts} />}
					/>
					<Route path="/add_new" component={() => <AddPost parentCallback={this.callBackFunction} />} />
					<Route
						path="/edit/:id"
						component={() => (
							<AddPost posts={this.state.posts} parentEditCallback={this.callBackEditFunction} />
						)}
					/>
				</Router>
			</React.Fragment>
		);
	}
}

export default Project_B;
