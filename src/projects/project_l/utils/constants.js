export const menuList = [
    {
        id: '001',
        label: 'About',
        url: 'home/about'
    },
    {
        id: '002',
        label: 'What we do',
        url: 'home/what_we_do'
    },
    {
        id: '003',
        label: 'Our Clients',
        url: 'home/our_clients'
    },
    {
        id: '004',
        label: 'Contact Us',
        url: 'home/contact_us'
    }
]