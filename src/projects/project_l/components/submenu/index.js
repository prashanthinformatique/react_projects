import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { menuList } from '../../utils/constants';
const SubMenu = () => {

    return ( <>
        <div class="layer_2">
            <ul class="menu">
                {
                    menuList.map( element => <li key={element.id}>
                        <Link to={element.url}>{element.label}</Link>
                    </li> )
                }
            </ul>
        </div>
    </> );
}
 
export default SubMenu;