import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link, useRouteMatch, Redirect } from 'react-router-dom';
import SubMenu from "../../submenu";
import AboutUs from "../about_us";
import WhatWeDo from "../what_we_do";


const Home = () => {

    let { path, url } = useRouteMatch();

    console.log(`url: ${url}, path: ${path}  `);

    return ( <>
        <h3>Home</h3>
        {/* <SubMenu />  */}
        <div class="layer_2">
            <ul class="menu">
                <li>
                    <Link to={`${url}/about`}>About Us</Link>
                </li>
                <li>
                    <Link to={`${url}/what_we_do`}>What We Do</Link>
                </li>
            </ul>
        </div>


        <Switch>
            {/* <Route path={`${path}`}>
                <AboutUs />
            </Route> */}
            <Route path={`${path}/about`}>
                <AboutUs />
            </Route>
            <Route path={`${path}/what_we_do`}>
                <WhatWeDo />
            </Route>
            <Route exact path={`${path}`}>
                <Redirect to={`${url}/about`} /> 
            </Route>
        </Switch>
    </> );
}
 
export default Home;
