import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link, useRouteMatch } from 'react-router-dom';
import Home from "./components/views/home";
import Topics from "./components//views/topics";
import Invoices from "./components/views/invoices";
import Settings from "./components/views/settings";

// import SubMenu from "./components/submenu";
import AboutUs from "./components/views/about_us";
import WhatWeDo from "./components/views/what_we_do";


import './style.scss';

const ProjectL = () => {

    // let { path, url } = useRouteMatch();


    
    console.log( window.location );


	return (
		
            <Router>

                <div className="layer_1">
                    <ul className="menu">
                        <li>
                            <Link to="/home">Home</Link>
                        </li>
                        <li>
                            <Link to="/topics">Topics</Link>
                        </li>
                        <li>
                            <Link to="/invoices">Invoices</Link>
                        </li>
                        <li>
                            <Link to="/settings">Settings</Link>
                        </li>
                    </ul>
                </div>

                <Switch>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/topics">
                        <Topics />
                    </Route>
                    <Route path="/invoices">
                        <Invoices />
                    </Route>
                    <Route path="/settings">
                        <Settings />
                    </Route>
                </Switch>
                
            
            </Router>
		
	);
};

export default ProjectL;
