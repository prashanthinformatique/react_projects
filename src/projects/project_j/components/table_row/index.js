import React from 'react';
import { USERS_PER_PAGE } from '../../utils/constants';

const TableRow = (props) => {

    const { data, page } = props;
    const startIndex = (page - 1) * USERS_PER_PAGE;
    const selectedData = data.slice(startIndex, startIndex + USERS_PER_PAGE);

    return ( <>
        { selectedData.map( row => 
             <li key={row.jobid} className="table-row">
                <div className="col col-1" data-label="Job Id">{row.jobid}</div>
                <div className="col col-2" data-label="Customer Name">{row.customer_name}</div>
                <div className="col col-3" data-label="Amount">{row.amount_due}</div>
                <div className="col col-4" data-label="Payment Status">{row.payment_status}</div>
            </li>
        ) }
    </> );
}
 
export default TableRow;