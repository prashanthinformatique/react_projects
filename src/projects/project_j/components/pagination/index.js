import React from 'react';

const Pagination = (props) => {

    const { totalPages, handleClick, page, handleNext, handlePrev, pageShorten } = props;

    const pages = pageShorten;

    return ( 
        <ul className="pagination">
            <li><span onClick={ () => handlePrev() } className="prev">{`<`} Prev</span></li>
            {
                pages.map( number => (
                    <li 
                    key={number} 
                    onClick={ () => handleClick(number) } 
                    className={`pageNumber ${number === page && 'active'}`}>
                    <span>{number}</span>
                    </li>)
                )
            }
            <li><span onClick={ () => handleNext() }  className="next">Next {`>`}</span></li>
        </ul>
     );
}
 
export default Pagination;