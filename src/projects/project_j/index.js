import React, { useState, useEffect } from "react";
import Pagination  from "./components/pagination";
import TableRow from "./components/table_row";
import './style.scss';
import { USERS_PER_PAGE } from "./utils/constants";

const ProjectJ = () => {

	// const tableRow = [
	// 	{ 
	// 		jobid: '42235',
	// 		customer_name: 'john doe',
	// 		amount_due: '$350',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10112',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '14256',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '12589',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '42442',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '42257',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '42311',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10001',
	// 		customer_name: 'john doe',
	// 		amount_due: '$350',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10002',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10003',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10004',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10005',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10006',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10007',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10008',
	// 		customer_name: 'john doe',
	// 		amount_due: '$350',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10009',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10010',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10011',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10012',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10013',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10014',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10015',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10016',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10017',
	// 		customer_name: 'Jennifer Smith',
	// 		amount_due: '$220',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10018',
	// 		customer_name: 'John Smith',
	// 		amount_due: '$341',
	// 		payment_status: 'pending'
	// 	},
	// 	{ 
	// 		jobid: '10019',
	// 		customer_name: 'John Carpenter',
	// 		amount_due: '$115',
	// 		payment_status: 'pending'
	// 	}
	// ]

	// useEffect(()=>{
	// 	console.log( paginationShorten( page, totalPages ) )
	// 	setTotalPages( paginationShorten( page, totalPages ) );
	// })
	
	const [ tableRow, setTableRow ] = useState(
		[
			{ 
				jobid: '42235',
				customer_name: 'john doe',
				amount_due: '$350',
				payment_status: 'pending'
			},
			{ 
				jobid: '10112',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '14256',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '12589',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '42442',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '42257',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '42311',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '10001',
				customer_name: 'john doe',
				amount_due: '$350',
				payment_status: 'pending'
			},
			{ 
				jobid: '10002',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '10003',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '10004',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '10005',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '10006',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '10007',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '10008',
				customer_name: 'john doe',
				amount_due: '$350',
				payment_status: 'pending'
			},
			{ 
				jobid: '10009',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '10010',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '10011',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '10012',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '10013',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '10014',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '10015',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '10016',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			},
			{ 
				jobid: '10017',
				customer_name: 'Jennifer Smith',
				amount_due: '$220',
				payment_status: 'pending'
			},
			{ 
				jobid: '10018',
				customer_name: 'John Smith',
				amount_due: '$341',
				payment_status: 'pending'
			},
			{ 
				jobid: '10019',
				customer_name: 'John Carpenter',
				amount_due: '$115',
				payment_status: 'pending'
			}
		]
	)

	const [totalPages , setTotalPages] =  useState( Math.ceil(tableRow.length / USERS_PER_PAGE) );

	const [ page, setPage ] = useState(1);

	const paginationShorten = (c, m) => {
        var current = c,
            last = m,
            delta = 2,
            left = current - delta,
            right = current + delta + 1,
            range = [],
            rangeWithDots = [],
            l;
    
        for (let i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
    
        for (let i of range) {
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                } else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
    
        return rangeWithDots;
    }


	const [ pageShorten, setPageShorten ] = useState(paginationShorten( page, totalPages ));



	const handleClick = number => {
		setPage(number);
		setPageShorten(paginationShorten( number, totalPages ))
	};

	const handleNext = () => {
		setPage(page < totalPages ? page+ 1 : page );
		setPageShorten(paginationShorten( page , totalPages ))
	}
	const handlePrev = () => {
		setPage(page - 1 > 0 ? page - 1 : page );		
		setPageShorten(paginationShorten( page, totalPages ))
	}

	

	return (
		<React.Fragment>
           <div className="container">
				<h2>Responsive Tables Using LI <small>Triggers on 767px</small></h2>
				<ul className="responsive-table">
					<li className="table-header">
						<div className="col col-1">Job Id</div>
						<div className="col col-2">Customer Name</div>
						<div className="col col-3">Amount Due</div>
						<div className="col col-4">Payment Status</div>
					</li>
					<TableRow data={tableRow} page={page} />
				</ul>
			</div>


			<Pagination 
				 totalPages={totalPages}
				 handleClick={handleClick}
				 handleNext={handleNext}
				 handlePrev={handlePrev}
				 page={page}
				 pageShorten={ pageShorten }
			/>

		</React.Fragment>
	);
};

export default ProjectJ;
