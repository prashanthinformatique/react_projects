import React, { useState, useEffect } from "react";

import ScrollArea from "react-scrollbar";
import "./global.scss";

const ProjectF = () => {
	return (
		<React.Fragment>
			<header></header>

			<div className="body_layout">
				<div className="sidebar"></div>
				<div className="inner_sidebar"></div>
				<div className="content_area">
					<ScrollArea
						speed={0.8}
						className="area"
						contentClassName="content"
						horizontal={false}
					>
						<div className="ca_area">
							<h2>
								Test dataTest dataTest dataTest dataTest
								dataTest data
							</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
							<h2>Test data</h2>
						</div>
					</ScrollArea>
				</div>
			</div>
		</React.Fragment>
	);
};

export default ProjectF;
