import React, { useState, useEffect, useMemo } from "react";
import './style.scss';

const ProjectM = () => {

	const [ number, setNumber ] =  useState(1);

	const [ inc, setInc ] = useState(0);


	// By Using Below Line , Whenever component gets re-rendered Below Statment was called and console is Triggered
	// const factorial = Factortial(number);
	
	// By using UseMemo : 1s time for new value alone , below function will be called. So If no change occured in 
	// input field means, below function won't be called. You can check By clicking on Re-render button 
	const factorial = useMemo( () => 
		Factortial(number), [number] );



	const handleChange = (e) => {
		setNumber( Number(e.target.value) );
	}

	const handleClick = () => {
		setInc( i => i + 1 );
	}


	return (
		<div>
				<input type="text" name="fact" id="fact" value={number} onChange={handleChange} />
				<button onClick={handleClick}>Re-render</button>
				<br />
				Result is {factorial}
		</div>
	);
};

export default ProjectM;


function Factortial( n ){
	console.log("Factorial of N Callled");
	return n<=0 ? 1 : n * Factortial(n-1);
}
