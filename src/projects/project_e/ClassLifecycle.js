import React, { Component } from "react";

class ProjectE extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: "Hello World",
			flag: true,
		};
		console.log("Constructor called...", this.state);

		this.updateValue = this.updateValue.bind(this);
	}

	// static getDerivedStateFromProps(props, state) {
	// 	console.log("getDerivedStateFromProps Called...", props);

	// 	return {
	// 		value: props.data,
	// 	};
	// }

	componentDidMount() {
		console.log("ComponentDidMount Called...");

		// return;
		// setTimeout(() => this.tick(), 2000);
	}

	tick() {
		if (this.state.value === "Hello Biznesscode") {
			this.setState({
				value: "",
			});
		} else {
			this.setState({
				value: "Hello Biznesscode",
			});
		}
	}

	shouldComponentUpdate() {
		let status = true;
		console.log("shouldComponentUpdate Called...", status);
		return status;
		// if (this.state.flag === false) {
		// 	return false;
		// } else {
		// 	this.setState({
		// 		flag: false,
		// 	});
		// 	return true;
		// }
	}

	getSnapshotBeforeUpdate(prevProps, prevState) {
		console.log(
			"getSnapShotBeforeUpdate called...Previous state value",
			prevState
		);
	}
	componentDidUpdate() {
		console.log(
			"componentDidUpdate called...New State Value",
			this.state.value
		);
	}

	updateValue = () => {
		this.setState({
			value: "Hello Biznesscode",
		});
		console.log("Update Value Method", this.state);
	};

	render() {
		console.log("render called...", this.state);
		return (
			<>
				<h5>{this.state.value}</h5>
				<button onClick={this.updateValue}>Update</button>
			</>
		);
	}
}

export default ProjectE;
