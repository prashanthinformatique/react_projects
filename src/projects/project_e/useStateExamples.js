import React, { useState } from "react";

// For render the state value into component only once.. use () function when defining the 'value' inside the state
function initialState() {
	console.log("Update State Value into component..");
	return "Welcome";
}

const ProjectE = () => {
	const [heading, setHeading] = useState(() => initialState());
	const [count, setCount] = useState(4);

	const [state, setState] = useState({ rollno: 1, theme: "biznesscode" });

	const rollno = state.rollno;
	const theme = state.theme;

	const button = {
		display: "block",
		padding: "10px 20px",
		margin: "0 auto",
		marginTop: "20px",
		cursor: "pointer",
	};

	// Update a string
	function updateHeading() {
		// setHeading((prevHeading) => {
		// 	return prevHeading + " Welcome to Biznesscode";
		// });
		setHeading("Welcome to Biznesscode");
	}

	const Increment = () => {
		setCount((prevCount) => prevCount + 1);
	};

	// Update the object value into the state
	const updateValue = () => {
		setState((prevState) => {
			return { ...prevState, rollno: prevState.rollno + 1 };
		});
	};

	return (
		<div>
			<h3 style={{ textAlign: "center" }}>{heading}</h3>
			<button style={button} onClick={updateHeading}>
				Explore
			</button>

			<h3 style={{ textAlign: "center", marginTop: "30px" }}>{count}</h3>
			<button style={button} onClick={Increment}>
				Increment
			</button>

			<h3 style={{ textAlign: "center", marginTop: "30px" }}>
				{rollno}&nbsp;&nbsp;{theme}
			</h3>
			<button style={button} onClick={updateValue}>
				Update Value
			</button>
		</div>
	);
};

export default ProjectE;
