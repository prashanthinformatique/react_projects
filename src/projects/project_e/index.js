import React, { useState, useEffect } from "react";

const ProjectE = () => {
	const [count, setCount] = useState(4);

	const [windowWidth, setWindowWidth] = useState(window.innerWidth);

	const handleResize = () => {
		setWindowWidth(window.innerWidth);
	};

	useEffect(() => {
		console.log("Count Changed");

		window.addEventListener("resize", handleResize);

		return () => {
			console.log(count);
			console.log("Return from Count...");
		};
	}, [count, windowWidth]);

	const button = {
		display: "block",
		padding: "10px 20px",
		margin: "0 auto",
		marginTop: "20px",
		cursor: "pointer",
	};

	const Increment = () => {
		setCount((prevCount) => prevCount + 1);
	};

	return (
		<div>
			<h3 style={{ textAlign: "center" }}>{count}</h3>
			<button style={button} onClick={Increment}>
				Increment
			</button>

			<h3 style={{ textAlign: "center", marginTop: "30px" }}>
				{windowWidth}
			</h3>
		</div>
	);
};

export default ProjectE;
