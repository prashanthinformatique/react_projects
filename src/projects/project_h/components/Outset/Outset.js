import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

const Outset = ({ type, top, right, bottom, left, children }) => {
	const Tag = type === undefined ? "div" : type;
	const styles = {
		marginTop: top,
		marginRight: right,
		marginBottom: bottom,
		marginLeft: left,
	};
	// const sheet = new CSSStyleSheet();

	return (
		<>
			<Tag style={styles}>{children}</Tag>
		</>
	);
};

Outset.propTypes = {
	/* Margin Top */
	top: PropTypes.number,
	/* Margin Right */
	right: PropTypes.number,
	/* Margin Bottom */
	bottom: PropTypes.number,
	/* Margin left */
	left: PropTypes.number,

	/* Tag for Wrapper : Optional */
	type: PropTypes.string,
};

export default Outset;
