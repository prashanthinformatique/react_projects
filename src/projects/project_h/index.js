import React, { useState, useEffect } from "react";
import Outset from "./components/Outset/Outset";

const ProjectH = () => {
	return (
		<React.Fragment>
			<h4>Story Book</h4>
			<Outset top={20} right={20} bottom={20} left={20}>
				Heading
			</Outset>
		</React.Fragment>
	);
};

export default ProjectH;
