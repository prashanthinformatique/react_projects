import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

import MoviesInfo from './components/moviesInfo';
import MoviesList from './components/moviesList';
import '../../app.scss';
import './project_d.scss';

class Project_D extends Component {
	state = {
		value: 0,
		setValue: 0
	};

	handleChange = (event, newValue) => {
		this.setState({
			value: newValue
		});
	};

	componentDidMount() {
		fetch('http://www.omdbapi.com/?s=x%20men&y=2000&apikey=d60219e4', {
			method: 'get'
		})
			.then((response) => response.json())
			.then((jsonData) => {
				// console.log(`API Response : ${jsonData}`);
				this.setState({
					apiResponse: jsonData.Response,
					totalResults: jsonData.totalResults,
					search: jsonData.Search
				});
			});
	}

	render() {
		return (
			<React.Fragment>
				<div className="tabs">
					<AppBar position="static">
						<Tabs value={this.state.value} onChange={this.handleChange} aria-label="simple tabs example">
							<Tab label="Movie Info" />
							<Tab label="All movies" />
						</Tabs>
					</AppBar>
				</div>
				<div className="tabs_panel">
					<div value={this.state.value} hidden={this.state.value !== 0}>
						<MoviesInfo newData={this.state.search} />
					</div>
					<div value={this.state.value} hidden={this.state.value !== 1}>
						<MoviesList newData={this.state.search} />
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Project_D;
