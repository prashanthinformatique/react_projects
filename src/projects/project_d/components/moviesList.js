import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import dummyImage from '../../../assets/images/projects_d/dummy_image.jpg';

class MoviesList extends Component {
	state = {
		searchResults: [],
		searchResultsForAutocomplete: [],
		searchResultsForPoster: [],
		movieTitle: '',
		movieYear: ''
	};

	componentDidUpdate(prevProps) {
		// console.log(`ComponentDidUpdate :  ${this.props}`);
		// console.log(this.props.newData);

		if (this.props.newData !== prevProps.newData) {
			this.setState({
				searchResults: this.props.newData,
				searchResultsForAutocomplete: this.props.newData,
				searchResultsForPoster: this.props.newData
			});
		}
	}

	movieTitle = (event, value) => {
		// console.log(value);
		const { searchResults } = this.state;

		if (value != null) {
			this.setState({
				movieTitle: value.Title
			});
		} else {
			this.setState({
				movieTitle: '',
				searchResultsForPoster: searchResults
			});
		}
	};

	movieYear = (event, value) => {
		const { searchResults } = this.state;

		if (value != null) {
			this.setState({
				movieYear: value.Year
			});
		} else {
			this.setState({
				movieYear: '',
				searchResultsForPoster: searchResults
			});
		}
	};

	searchQuery = () => {
		const { movieTitle, movieYear, searchResultsForPoster } = this.state;

		const searchResultsForPoster_new = searchResultsForPoster.filter(
			(data) => data.Title === movieTitle && data.Year === movieYear
		);

		this.setState({
			searchResultsForPoster: searchResultsForPoster_new
		});
	};

	resetQuery = () => {
		const { searchResults } = this.state;

		this.setState({
			searchResultsForPoster: searchResults
		});
	};

	render() {
		const { searchResults, searchResultsForAutocomplete, searchResultsForPoster } = this.state;

		return (
			<React.Fragment>
				<div className="select_boxes">
					<Autocomplete
						id="combo-box-demo"
						size="small"
						className="movie_title"
						options={searchResultsForAutocomplete}
						getOptionLabel={(option) => option.Title}
						style={{ width: 300 }}
						renderInput={(params) => <TextField {...params} label="Movie Title" variant="outlined" />}
						onChange={this.movieTitle}
					/>

					<Autocomplete
						id="combo-box-demo"
						size="small"
						className="movie_title"
						options={searchResultsForAutocomplete}
						getOptionLabel={(option) => option.Year}
						style={{ width: 300 }}
						renderInput={(params) => <TextField {...params} label="Movie Year" variant="outlined" />}
						onChange={this.movieYear}
					/>

					<Button className="search_btn" variant="contained" color="primary" onClick={this.searchQuery}>
						Search
					</Button>

					<Button className="search_btn" variant="contained" color="secondary" onClick={this.resetQuery}>
						Reset
					</Button>
				</div>

				<div className="gridList">
					{searchResultsForPoster.map((item) => (
						<GridList key={item.imdbID} cellHeight={180} className="grid_list">
							<GridListTile>
								<img src={item.Poster == 'N/A' ? dummyImage : item.Poster} />
								<GridListTileBar
									title={item.Title}
									actionIcon={
										<IconButton className="icon_color">
											<InfoIcon />
										</IconButton>
									}
								/>
							</GridListTile>
						</GridList>
					))}
				</div>
			</React.Fragment>
		);
	}
}

export default MoviesList;
