import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import dummyImage from '../../../assets/images/projects_d/dummy_image.jpg';

class MoviesInfo extends Component {
	state = {
		page: 0,
		setPage: 0,
		rowsPerPage: 5,
		setRowsPerPage: 5,
		table: [
			{ id: 1, row: 'row 1' },
			{ id: 2, row: 'row 2' },
			{ id: 3, row: 'row 3' },
			{ id: 4, row: 'row 4' },
			{ id: 5, row: 'row 5' }
		],
		open: false,
		setOpen: false,
		searchResults: [],
		searchResultsForAutocomplete: [],
		searchResultsForTable: [],
		singleMovieResponse: {},
		rating: true,
		movieTitle: '',
		movieYear: ''
	};

	handleChangePage = (event, newPage) => {
		// console.log(`New page ${newPage}`);
		this.setState({
			setPage: newPage,
			page: newPage
		});
	};
	handleChangeRowsPerPage = (event) => {
		this.setState({
			setRowsPerPage: +event.target.value,
			rowsPerPage: +event.target.value,
			setPage: 0
		});
	};

	handleClose = () => {
		this.setState({
			open: false,
			setOpen: false
		});
	};

	handleOpen = (imdbID) => {
		// console.log(imdbID);

		fetch('http://www.omdbapi.com/?i=' + imdbID + '&plot=full&apikey=d60219e4', {
			method: 'get'
		})
			.then((response) => response.json())
			.then((jsonData) => {
				// console.log(jsonData);
				this.setState({
					singleMovieResponse: jsonData
				});
				this.setState({
					rating: this.state.singleMovieResponse.imdbRating > 7 ? true : false
				});
			});

		this.setState({
			open: true,
			setOpen: true
		});
	};

	componentDidUpdate(prevProps) {
		// console.log(`ComponentDidUpdate :  ${this.props}`);
		// console.log(this.props.newData);

		if (this.props.newData !== prevProps.newData) {
			this.setState({
				searchResults: this.props.newData,
				searchResultsForAutocomplete: this.props.newData,
				searchResultsForTable: this.props.newData
			});
		}
	}

	movieTitle = (event, value) => {
		// console.log(value);
		const { searchResults } = this.state;

		if (value != null) {
			this.setState({
				movieTitle: value.Title
			});
		} else {
			this.setState({
				movieTitle: '',
				searchResultsForTable: searchResults
			});
		}
	};

	movieYear = (event, value) => {
		const { searchResults } = this.state;

		if (value != null) {
			this.setState({
				movieYear: value.Year
			});
		} else {
			this.setState({
				movieYear: '',
				searchResultsForTable: searchResults
			});
		}
	};

	searchQuery = () => {
		const { movieTitle, movieYear, searchResultsForTable } = this.state;

		const searchResultsForTable_new = searchResultsForTable.filter(
			(data) => data.Title === movieTitle && data.Year === movieYear
		);

		this.setState({
			searchResultsForTable: searchResultsForTable_new
		});
	};

	render() {
		const {
			page,
			rowsPerPage,
			table,
			open,
			searchResults,
			searchResultsForAutocomplete,
			searchResultsForTable,
			rating
		} = this.state;

		// console.log(searchResults);
		return (
			<React.Fragment>
				<div className="select_boxes">
					<Autocomplete
						id="combo-box-demo"
						size="small"
						className="movie_title"
						options={searchResultsForAutocomplete}
						getOptionLabel={(option) => option.Title}
						style={{ width: 300 }}
						renderInput={(params) => <TextField {...params} label="Movie Title" variant="outlined" />}
						onChange={this.movieTitle}
					/>

					<Autocomplete
						id="combo-box-demo"
						size="small"
						className="movie_title"
						options={searchResultsForAutocomplete}
						getOptionLabel={(option) => option.Year}
						style={{ width: 300 }}
						renderInput={(params) => <TextField {...params} label="Movie Year" variant="outlined" />}
						onChange={this.movieYear}
					/>

					<Button className="search_btn" variant="contained" color="primary" onClick={this.searchQuery}>
						Search
					</Button>
				</div>

				<div className="table_design">
					<Table stickyHeader aria-label="sticky table">
						<TableHead>
							<TableRow>
								<TableCell align="center">Poster</TableCell>
								<TableCell align="center">Title</TableCell>
								<TableCell align="center">Type</TableCell>
								<TableCell align="center">Year</TableCell>
								<TableCell align="center">imdb ID</TableCell>
								<TableCell align="center">Info Details</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{searchResultsForTable
								.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
								.map((row) => (
									<TableRow key={row.imdbID} id={row.imdbID} hover>
										<TableCell align="center">
											<img src={row.Poster == 'N/A' ? dummyImage : row.Poster} />
										</TableCell>
										<TableCell align="center">{row.Title}</TableCell>
										<TableCell align="center">{row.Type}</TableCell>
										<TableCell align="center">{row.Year}</TableCell>
										<TableCell align="center">{row.imdbID}</TableCell>
										<TableCell align="center">
											<Button
												variant="contained"
												color="secondary"
												onClick={() => this.handleOpen(row.imdbID)}
											>
												Info
											</Button>
										</TableCell>
									</TableRow>
								))}
						</TableBody>
					</Table>
					<TablePagination
						rowsPerPageOptions={[ 5, 8, 15 ]}
						component="div"
						count={searchResultsForTable.length}
						rowsPerPage={rowsPerPage}
						page={page}
						onChangePage={this.handleChangePage}
						onChangeRowsPerPage={this.handleChangeRowsPerPage}
					/>
				</div>

				<Modal
					aria-labelledby="transition-modal-title"
					aria-describedby="transition-modal-description"
					className="box_modal"
					open={open}
					onClose={this.handleClose}
					closeAfterTransition
					BackdropComponent={Backdrop}
					BackdropProps={{
						timeout: 500
					}}
				>
					<Fade in={open}>
						<div className="paper">
							<h4 id="transition-modal-title">Box Office</h4>
							<h3 id="transition-modal-description">{rating == true ? 'Hit' : 'Flop'}</h3>
						</div>
					</Fade>
				</Modal>
			</React.Fragment>
		);
	}
}

export default MoviesInfo;
